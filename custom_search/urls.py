from django.urls import path

from .views import IndexView,get_tweetdata
app_name = 'custom_search'

urlpatterns = [
    path(
        '',
        IndexView.as_view(),
        name='index',
        ),
    path(
        'get_tweetdata/',
        get_tweetdata,
        name='get_tweetdata',
        ),
]
