# *************************************************************************************************************************************************************
# This python file is a view file for the custom_search app. It renders all the needed data in index.html and load_data.html which are under templates/custom_search. 
# *************************************************************************************************************************************************************
from django.shortcuts import render
from .custom_tweet_collector import collect_tweets_for_product
from multiprocessing import Process
import time
import multiprocessing
import heapq
from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from main_project.settings import STATIC_DIR
from datetime import date, timedelta
import datetime
import random
from django.utils.safestring import mark_safe
from django.template import Context
from django.template.context import make_context
import re
import urllib.request
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
# *************************************************************************************************************************************************************
class IndexView(TemplateView):
    template_name = 'custom_search/index.html'

#     Function :
#                   get_context_data(**kwargs): Returns the context that is needed to display in "custom_search/index.html"
#     Input : 
#                   None
#     Output :      
#                   Returns the context needed to display in "custom_search/index.html"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Generates context for the blurb
        # context["statistical_data"] = collect_tweets_for_product()
        # Generates context for the about me section

        return context

# *************************************************************************************************************************************************************
#     Function :
#                   get_tweetdata(request): It collects the twitter statistical data for any keyword by calling function collect_tweets_for_product() renders the load_data.html with this statistical data and returns this as a response to the ajax script which called get_tweetdata from index.html
#     Input : 
#                   request :
#     Output :      
#                   Renders the "custom_search/load_data.html" with context as statistical twitter data for the keywords.
# 	  Note : 		
# 					Here load_data.html is just a collection of div containers and not a comple html doc. So as soon as context is renderd in load_data.html it is dynamically placed appropriately inside index.html.

def get_tweetdata(request):
	# print ("Call to get_tweetdata is successfull")
	context = {}
	# try:
	# 	product_name = request.POST['product_name']
	# 	context["statistical_data"] = collect_tweets_for_product(product_name=product_name)
	# 	context["success_message"] = "Product found"
	# except:
	# 	context["success_message"] = "Databse server error! Product can't be found."

	if request.method == 'POST' and request.is_ajax():
		product_name = request.POST['product_name']
		print ("im here")
		context["statistical_data"] = collect_tweets_for_product(product_name=product_name)
		context["success_message"] = "Product found"
	else:
		print (product_name)
		context["success_message"] = "Databse server error! Product can't be found."

	return render(request,'custom_search/load_data.html',context)

# *************************************************************************************************************************************************************
