# ****************************************************************************************************************************************************************************************************
# This file contains the utility functions on for collecting the twitter data via twitter earch rest api and processing it. These functions are called by function in views file of custom_search app.
# ****************************************************************************************************************************************************************************************************
import tweepy  
import time as code_time
import json
import sys
from pprint import pprint
import datetime
import logging
import random
# from lang_named_er import lang_ner
# from lang_sentiment_predictor import lang_sentiment_pred
from multiprocessing import Process
import time
import multiprocessing
import heapq
from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from main_project.settings import STATIC_DIR
from datetime import date, timedelta
import datetime
import random
from django.utils.safestring import mark_safe
from django.template import Context
from django.template.context import make_context
import re
import urllib.request
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
import json
import heapq

# *************************************************************************************************************************************************************

# These are the main functions which are called from the view to support it:
#  1. Class tweet_collector -> Function collect_tweet_by_period
#  2. function analyse_parameters
#  3. function collect_tweets_for_product
# So to start understanding the code please start from understanding the above.

# *************************************************************************************************************************************************************


# The Max_heap class is a max heap for storing the values accoring to their key. Like say for value ("SMS DATA TECH") with key 34, where 34 representing its priority level.

class Max_heap:

	def __init__(self):
		self.H = []
		heapq.heapify(self.H)
# ************************************************************************************************************************************************************************
#     Function :
# 					push(key,value)
#     Input : 
#     				key : This is basically a priority level which decides which would be the max element.
#     				value : This is represents the element which would be stored in heap. Like say value can be ("Japan") or ("India") represeting particular info.  
#     Output :
# 					None
 
	def push(self,key,value):
		heapq.heappush(self.H,(-1*key,value))

 # ************************************************************************************************************************************************************************
#     Function :
# 					pop()
#     Input : 
#     				None
#     Output :
#     				None
 


	def pop(self):
		heapq.heappop(self.H)

 # ************************************************************************************************************************************************************************
#     Function :
# 					get(number_of_elements = n)
#     Input : 
#     				number_of_elements : number of top elements to be returned from heap.  		
#     Output :
#     				List of elements where each element is a tuple of (value,key). 
#     Notes : 
#					It has been implemented in different way to hande duplicate values. 

# ************************************************************************************************************************************************************************
	def get(self,number_of_elements = 1):
		count = 1
		item_list = []
		duplicate_dict = {}
		while len(self.H)>0 :
			item = self.H[0]
			heapq.heappop(self.H)

			if count > number_of_elements:
				break

			if item[1] not in duplicate_dict:
				duplicate_dict[item[1]]=1
				count += 1
				key = -1*item[0]
				value = item[1]
				item_list.append((value,key))
			else :
				continue
            
			# print (item_list)
		return item_list


# ************************************************************************************************************************************************************************

#     Class :
# 					tweetcollector():It collects the twitter tweets for a product using a self function collect_tweets_by_period and processes and analyse them via other self atrributes and methods. 
#     Input : 
#     				None	
#     Output :
#     				None 
 

class tweetCollector:


	def __init__(self):
		self.apis = []
		self.apis.extend([self.api_one(),self.api_two(),self.api_one(),self.api_two(),self.api_one(),self.api_two(),self.api_one()])
		self.api_count = 1
		self.max_id = -10000
		# self.ner = lang_ner()
		# self.data_dict = []
		# self.sentiment_predictor = lang_sentiment_pred()
# ************************************************************************************************************************************************************************
#  Function : 
# 				Following below are the list of twitter apis used for scraping the datat from twitter 
 
	def api_one(self):
		access_token="854971161066721280-K1HA0f5Lnl0qEj1kyjlx1RX9AJjiXcU"
		access_token_secret="JqJsTJp30LahQCZxdxgS5Qv75AlJcjim8G97NtuHcJdqd"
		consumer_key="NidXAgtCdSj5n9Yy21pwK7nvu"
		consumer_secret="JKlIii22j3huhLmE6UDFxLw9uZZvdCxi95tHzbUJYniyyh0cYs"

		auth = tweepy.OAuthHandler(consumer_key, consumer_secret) 
		auth.set_access_token(access_token, access_token_secret)
		api  = tweepy.API(auth)
		return api

	def api_two(self):
		access_token="854971161066721280-9GqFRiPOgJKeMcO11yGV8MAZ2didAnW"
		access_token_secret="o5qCo6ounQDE9LUsYLxwDfJcvMpUB8cSwi9BvHajSqjpr"
		consumer_key="ewEv7jSYxCTtcdKELor4cEEx7"
		consumer_secret="MQwYvmQ7vaPPb6Dp5ltGVpOwuoJD0sx2UCOnH85t5pEXEoOakQ"

		auth = tweepy.OAuthHandler(consumer_key, consumer_secret) 
		auth.set_access_token(access_token, access_token_secret)
		api  = tweepy.API(auth)
		return api
# ************************************************************************************************************************************************************************
#     Function :
# 					get_tweet_info(): It collects only specific paramaeters from tweet JSON and then returs them as a dict for further processing.
#     Input : 
#     				tweet: It is JSON data of a twitter tweet.
#     Output :
#     				returns a dict containing the necessary parameters about a tweet.
# 	  Note :     
# 					Here parameters "sentiment" and "named_entities" are for now not implemented, beacause while sentiment analysis considerably lengthen the time of processing the tweets while the NER is not working due to multiprocessing. SO please see for future implementations.  
 
	def get_tweet_info(self,tweet):
		tweet_dict = {}
		tweet_dict["tweet_text"] = tweet.text
		tweet_dict["created_at"] = str(tweet.created_at)
		try:
			tweet.retweeted_status
			tweet_dict["is_retweeted"] = True
		except:
			tweet_dict["is_retweeted"] = False
		tweet_dict["retweet_count"] = tweet.retweet_count
		tweet_dict["favorite_count"]= tweet.favorite_count
		tweet_dict["language"] = tweet.lang
		tweet_dict["sentiment"] = random.uniform(0,1)
		tweet_dict["named_entities"] = []
		# print(tweet_dict["named_entities"])
		return tweet_dict

# ************************************************************************************************************************************************************************

#     Function :
# 					returns the info of user which tweeted the tweet in form of tuple.
#     Input : 
#     				tweet : It is JSON data of a twitter tweet
#     Output :
#     				returns tuple containing necessary info about user.
#     Note : 
#					Here its is important to convert the user info to tuple because later we have to push user_info into heap and dict can't be push in heap as value. Only tuples can.

	def get_user_info(self,tweet):
		user_name = tweet.user.screen_name
		user_follower_count  = tweet.user.followers_count
		user_profile_link = tweet.user.profile_image_url_https
		user_description = tweet.user.description
		return (user_name,user_follower_count,user_profile_link,user_description)
# ************************************************************************************************************************************************************************
#     Function :
# 					Reads the list of tuples returned from get function of Max_heap and convert them into dict form so that they can be push to elastic search. 
#     Input : 
#     				item_list : Contains the list of tuples where each tuple representing an user with its info.
#     Output :
#     				returns list of dict containing list of dict where each dict representing an user with its info 


	def convert_to_dict_1(self,item_list):
		result_dict =[]
		for item in item_list:
			user_dict = {}
			user_dict["name"] = item[0][0]
			user_dict["followers_count"] = item[0][1]
			user_dict["profile_link"] = item[0][2]
			user_dict["description"] = item[0][3]
			user_dict["value"] = item[1]
			result_dict.append(user_dict)
		return result_dict
# ************************************************************************************************************************************************************************
#     Function :
# 					Reads the list of tuples returned from get function of Max_heap and convert them into dict form so that they can be push to elastic search. 
#     Input : 
#     				item_list : Contains the list of tuples where each tuple representing a named entitiy with its info.
#     Output :
#     				returns list of dict containing list of dict where each dict representing a named entitiy with its info 

	def convert_to_dict_2(self,item_list):
		result_dict =[]
		for item in item_list:
			entity_dict = {}
			entity_dict["entity_name"] = item[0]
			entity_dict["entity_frequency"] = item[1]
			result_dict.append(entity_dict)
		return result_dict
# ************************************************************************************************************************************************************************

#     Function :
# 					It processes the tweets for a particular product for particular period or say time. and stores them in result_dict  	
#     Input : 
#     				product_name : Name of the product  
#   				product_index_name : used for building the indexname of the index in which data will be stored. We need this because indexname can only contain lowercase and undersore. 
#   				period : It is an integer, and describes for how many day tweets needed to be colleceted, So if say period = 7  this menas tweets will be colleceted for seven days.
#   				last_day : It is an integer, and describes from which dy before tweets needed to be colleceted, So if say last_day = 3 this means tweets should collected on before three days ago. 
#   				api_index : To describe which api from list self.apis should be used to scrape data.
#   				return_dict : It is dict in which processed statistical data will be stored in dict form
#     Output :
#     				None

	def collect_tweet_by_period(self,product_name,product_index_name,period,last_day,api_index,return_dict):
		for i in range(period):
			today = datetime.datetime.today()
			start_date = today - datetime.timedelta(days = last_day+i)
			end_date = today - datetime.timedelta(days = last_day-1+i)
			start_date = start_date.strftime("%Y-%m-%d")
			end_date = end_date.strftime("%Y-%m-%d")
			api = self.apis[api_index]
			positive_count = 0
			negative_count = 0
			neutral_count = 0
			original_count = 0
			retweet_count =  0
			retweet_per_tweet_count = 0
			likes_per_tweet_count = 0
			total_count = 0
			most_active = Max_heap()
			most_popular = Max_heap()
			most_entities = Max_heap()
			active_user_dict = {}
			entites_dict = {}
			try:
				for tweet in tweepy.Cursor(api.search,q=product_name,since = start_date,until = end_date,lang='ja').items(100):
					tweet_dict = self.get_tweet_info(tweet)
					# print ("original tweet : ",tweet_dict["tweet_text"])
					# print ("Named Entities",tweet_dict["named_entities"])
					if tweet_dict["sentiment"]>=0.4:
						positive_count += 1
					elif tweet_dict["sentiment"]<=0.1:
						negative_count += 1
					else:
						neutral_count += 1


					if tweet_dict["is_retweeted"]:
						retweet_count += 1
					else:
						original_count += 1

					likes_per_tweet_count += tweet_dict["favorite_count"]
					retweet_per_tweet_count += tweet_dict["retweet_count"]
					# print (tweet_dict)
					user_info = self.get_user_info(tweet)
					
					if user_info not in active_user_dict:
						active_user_dict[user_info] = 1
					else:
						active_user_dict[user_info] += 1

					for entity in tweet_dict["named_entities"]:
						for word in product_name.split(' '):
							if word.lower() not in entity.lower():
								if entity not in entites_dict:
									entites_dict[entity] = 1
								else :
									entites_dict[entity] += 1
								most_entities.push(entites_dict[entity],entity)
								break
							else:
								break



					most_active.push(active_user_dict[user_info],user_info)
					most_popular.push(user_info[1],user_info)
					total_count += 1
					# print (tweet_dict)
					# print ("tweets analysed .....:",total_count)
					
					# print (user_info)
					# self.es.push_to_elastic_serach(id_count=total_count,index=tweet_data_index_name,body=json.dumps(tweet_dict),doc_type = start_date)

			except tweepy.TweepError:
				print ("wait")
				code_time.sleep(60)
			statistical_dict = {}

			statistical_dict["positive_count"]=positive_count
			statistical_dict["negative_count"]=negative_count
			statistical_dict["neutral_count"]=neutral_count
			statistical_dict["retweet_count:"]=retweet_count
			statistical_dict["original_count"]=original_count
			statistical_dict["total_count"]=total_count
			statistical_dict["likes_per_tweet_count"]=likes_per_tweet_count
			statistical_dict["retweet_per_tweet_count"]=retweet_per_tweet_count
			statistical_dict["most_active"]=self.convert_to_dict_1(most_active.get(number_of_elements=50))
			statistical_dict["most_popular"]=self.convert_to_dict_1(most_popular.get(number_of_elements=50))
			statistical_dict["most_entities"]=self.convert_to_dict_2(most_entities.get(number_of_elements=50))
			return_dict[api_index] = statistical_dict
			# print (statistical_dict)
			# datalist.append(statistical_dict)
			# print (datalist)
			# for item in statistical_dict:
			#   	print (item,":",statistical_dict[item])
			# self.es.push_to_elastic_serach(id_count=1,index=tweet_statistical_data_index_name,body=json.dumps(statistical_dict),doc_type = start_date)
# ************************************************************************************************************************************************************************

#     Function :	
# 					get_average : A utility function for below analyse_parameter function. It takes the sum of all the values for list and list2 and then return the division of same.

def get_average(list1,list2):
    N = len(list1)
    numerator = 0
    denominator = 0
    for i in range(N):
        numerator += list1[i]
        denominator += list2[i]
    if denominator != 0:
        return float(numerator)/float(denominator)
    else:
        return 0
# *************************************************************************************************************************************************************

#     Function :	
# 					get_overall(main_list,number_of_elements,key_name,val_name,extend,knowledge): It reads through sublists present in main_list, pushes elements to heap while acting on certain input extend and knowledge, and then returns top elements on the basis of input number_of_elements.  
#     Input : 
#     				main_list : Its is the list of sub-list where each sub-list contains dict elements. ex:
# 					mainList = [
# 						[
# 							{"name":"Naruto","value":"45",...},
# 							{"name":"Sauske","value":"65",...},
# 							......
# 						],
# 						[

# 							{"name":"Tsunade","value":"49",...},
# 							{"name":"Madara","value":"55",...},
# 							......
# 						],
# 						..................
# 					]
# 					number_of_elements: It represents how many top elements are to be returned.
# 					key_name: It is a string which represents which element of dict will act as a key.
# 					val_name: It is a string which represents which element of dict represent that dict.
# 					extend: If it is true then while pushing in heap if element duplicate is found then the key values of all duplicate will be added else the maximum of the key value of duplicates will stored.
# 					knowledge: When elements are pushed in heap only key_name and val_name parameters are stored for a elemt in heap. So to restore there other properties if they have knowledge equal to true is kept else false is kept.   

#     Output :
#     				returns list of top elements

def get_overall(main_list=[],numbers_of_elements = 10,key_name = "value",val_name = "name",extend = True,knowledge = True):
    Heap = Max_heap()
    result_list = []
    duplicate_dict = {}
    knowledge_dict = {}
    for sublist in main_list:
        if sublist != None:
            for item in sublist:
                if knowledge:
                    if item[val_name] not in knowledge_dict:
                        knowledge_dict[item[val_name]] = item
                if extend:
                    if item[val_name] not in duplicate_dict:
                        duplicate_dict[item[val_name]] = item[key_name]
                    else:
                        duplicate_dict[item[val_name]] += item[key_name]
                else:
                    if item[val_name] not in duplicate_dict:
                        duplicate_dict[item[val_name]] = item[key_name]

                Heap.push(duplicate_dict[item[val_name]],item[val_name])

    top_elements = Heap.get(number_of_elements = numbers_of_elements)

    for items in top_elements:
        item_dict = knowledge_dict[items[0]]
        item_dict[key_name] = items[1]
        result_list.append(item_dict)
    
    return result_list
# *************************************************************************************************************************************************************

#     Function :	
# 					get_percentage : It is a utility function which returns list of dict items containing top entities depending upon number_of_elements variable 

def get_percentage(list_dict,numbers_of_elements = 7):
    result_list = []
    total = 0
    for item in list_dict:
        total += item["entity_frequency"]

    count  = 1

    for item in list_dict:
        if count > numbers_of_elements:
            break
        entity_dict = item
        if total != 0:
            entity_dict["frequencey_percentage"] = (float(item["entity_frequency"])/float(total))*100
        else:
            entity_dict["frequencey_percentage"] = 0
        result_list.append(entity_dict)
        count += 1
    return result_list

# *************************************************************************************************************************************************************


def get_dates(length,marksafe=True):
    dates = []
    for i in range(length):
        dt = date.today()-timedelta(i+1)
        # print (dt)
        dt = dt.strftime("%Y-%m-%d")
        dates.append(dt)
    if marksafe:
        dates = mark_safe(dates)
    return dates
# *************************************************************************************************************************************************************

def list_division(list1,list2):
	result_list = []
	for i,item in enumerate(list1):
		if list2[i] == 0:
			result_list.append(0)
		else:
			result_list.append(float(list1[i])/float(list2[i]))

	return result_list
# *************************************************************************************************************************************************************

def list_sum(sample_list):
	sum = 0
	for item in sample_list:
		sum += item

	return sum
# *************************************************************************************************************************************************************

#     Function :	
# 					analyse_parameters(statistical_data): It reads through all the data of each date and then provides result on the basis of overall statistical analysis.
#     Input : 
#     				statistical_data : It is the list of dict elements where each dict element represents the individual statistical data of each date.  
# 					mainList = [
# 						{	
# 							"total_count": 120,
# 							"positive_count": 70,
# 							"negative_count": 30,
# 							"neutral_count": 20,
# 							"retweet_count": 4556,
# 							..........
# 						},
# 						{
# 							......
# 						},
# 						.......
# 					]
#     Output :
#     				returns dict which contains numerous paramerts from statistical analysis on statistical_data


def analyse_parameters(statistical_data):
    total_count_list = []
    positive_count_list = []
    negative_count_list = []
    neutral_count_list = []
    retweet_count_list = []
    original_count_list = []
    most_active_users = []
    most_popular_users = []
    most_found_entities = []
    likes_per_tweet_count_list = []
    retweet_per_tweet_count_list = []
    for data in statistical_data:
        if data is not None:
            source_data = data
            # print (source_data)
            total_count_list.append(source_data["total_count"])
            positive_count_list.append(source_data["positive_count"])
            negative_count_list.append(source_data["negative_count"])
            neutral_count_list.append(source_data["neutral_count"])
            retweet_count_list.append(source_data["retweet_count:"])
            original_count_list.append(source_data["original_count"])
            most_active_users.append(source_data["most_active"])
            most_popular_users.append(source_data["most_popular"])
            most_found_entities.append(source_data["most_entities"])
            likes_per_tweet_count_list.append(source_data["likes_per_tweet_count"])
            retweet_per_tweet_count_list.append(source_data["retweet_per_tweet_count"])
        else:
            total_count_list.append(0)
            positive_count_list.append(0)
            negative_count_list.append(0)
            neutral_count_list.append(0)
            retweet_count_list.append(0)
            original_count_list.append(0)
            most_active_users.append(None)
            most_popular_users.append(None)
            most_found_entities.append(None)
            likes_per_tweet_count_list.append(0)
            retweet_per_tweet_count_list.append(0)


    statistical_data_dict = {}
    statistical_data_dict["total_tweets"] = list_sum(total_count_list)
    statistical_data_dict["dates_list"] = get_dates(7)
    statistical_data_dict["positive_count_list"] = positive_count_list
    statistical_data_dict["negative_count_list"] = negative_count_list
    statistical_data_dict["neutral_count_list"] = neutral_count_list
    statistical_data_dict["average_retweet_count_list"] = list_division(retweet_per_tweet_count_list,total_count_list)
    statistical_data_dict["average_likes_count_list"] = list_division(likes_per_tweet_count_list,total_count_list)
    statistical_data_dict["positive_count_percentage"] = round(get_average(positive_count_list,total_count_list)*100,2)
    statistical_data_dict["negative_count_percentage"] = round(get_average(negative_count_list,total_count_list)*100,2)
    statistical_data_dict["neutral_count_percentage"] = round(get_average(neutral_count_list,total_count_list)*100,2)
    statistical_data_dict["retweet_count_percentage"] = round(get_average(retweet_count_list,total_count_list)*100,2)
    statistical_data_dict["original_count_percentage"] = round(get_average(original_count_list,total_count_list)*100,2)
    statistical_data_dict["likes_per_tweet_count_percentage"] = round(get_average(likes_per_tweet_count_list,total_count_list),2)
    statistical_data_dict["retweet_per_tweet_count_percentage"] = round(get_average(retweet_per_tweet_count_list,total_count_list),2)
    statistical_data_dict["overall_active_users"] = get_overall(main_list = most_active_users,numbers_of_elements=5)
    statistical_data_dict["overall_popular_users"] = get_overall(main_list = most_popular_users,numbers_of_elements=5,extend=False)
    statistical_data_dict["overall_found_entities"] = get_percentage(get_overall(main_list = most_found_entities,numbers_of_elements=10,key_name = "entity_frequency",val_name="entity_name"))

    return statistical_data_dict
# *************************************************************************************************************************************************************

#     Function :	
# 					Collects twitter tweets for last week by creating seven processes and each process collects the tweet for a particular day and when all procesesses are done analyse_parameter() function is called to analyse on the data from the last last week tweets.
#     Input : 
#     				product_name : The keytag by which user want to search on twitter
# 					period : 
# 					last_day :
# 					
#     Output :
#     				returns dict which contains numerous paramerters from statistical analysis on statistical_data

def collect_tweets_for_product(product_name="Pocari Sweat",period=1,last_day=1):
	manager = multiprocessing.Manager()
	return_dict = manager.dict()
	product_index_name = "pocari_sweat"
	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")
	processes = list()
	period_length = 7
	period = 1
	print (datetime.datetime.today())
	for i in range(period_length):
		TC = tweetCollector()
		p = Process(target=TC.collect_tweet_by_period, args=(product_name,product_index_name,period,last_day+i,i,return_dict))
		processes.append(p)
		p.start()

	for index, p in enumerate(processes):
		p.join()
		# for i in range(period_length):
		# 	self.collect_tweet_by_period(product_name,product_index_name,period,last_day+i,i)

	print (datetime.datetime.today())
	statistical_data_dict = analyse_parameters(return_dict.values())
	print (statistical_data_dict)
	print (datetime.datetime.today())
	return statistical_data_dict

# *************************************************************************************************************************************************************
