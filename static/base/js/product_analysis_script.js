$(document).ready(function(){
  $('.positive-tweet-slider').slick({
    autoplay:true,
    autoplaySpeed:2000,
    slidesToShow: 3,
    dots:true,
    infinite:true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
        }
      }
    ]
  });
});

$(document).ready(function(){
  $('.active-user-slider').slick({
    autoplay:true,
    autoplaySpeed:2000,
    slidesToShow: 3,
    dots:true,
    infinite:true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
});

$(document).ready(function(){
  $('.slider').slick({
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 720,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
});

$(document).ready(function(){
  $('.subscribe-product-form').submit(function(e){
    e.preventDefault();
    var this_ = $(this);
    var urlEndpoint = this_.attr('action');
    var formData = this_.serialize();
    console.log(formData)
    $.ajax({
      url: urlEndpoint,
      method: "POST",
      data: formData,
      success: function(response){
        $('#collapse2').text(response['success_message']);
      },
    })
  })
})

function load_product_data(product_search_name){
  $('#Graphs-data').html("<div id='loading' style='width:100%;min-height:250px;height:auto';background:white;text-align:center><img id='loading-image' src='https://www.idlewild.org/wp-content/plugins/embed-bible-passages/images/ajax-loading.gif' alt='Loading...' style='display:block;margin-left:auto;margin-right:auto;align-self:center' /></div>")
  var formData = {'product_search_name':product_search_name};
  console.log(formData)
  $.ajax({
    url: "/profile/load_product_data/",
    method: "POST",
    data: formData,
    success: function(response){
    $('#Graphs-data').html(response)
    $('#Graphs-data').html(response)
     
    },
    error: function(response){
      console.log("sorry error occured");
    }
  });
}

function csv_send(){
  var data = new FormData();
  var file = null;
  var productname = $('#ProductList').find("option:selected").text();
  var datatypename = $('#DataTypeList').find("option:selected").text();
  console.log("yeah here")
  file = $("#csvfile")[0].files[0]

  //when uploading a file take the file
  $("#csvfile").on("change", function(){
    console.log("blewww")
      file = this.files
      console.log(file)
  });
  console.log(file);
  console.log(productname);
  //append the file in the data
  data.append("file", file);
  data.append("productname",productname)
  data.append("datatypename",datatypename)
  var urlEndpoint = $("#upload-form").attr('action');
  //append other data
  data.append("message", "some content");
  console.log(data)
  $.ajax({
      url: urlEndpoint,
      type: 'POST',
      data: data,
      contentType: false,
      processData: false,
      success: function (response) {
        $('#collapse3').text(response['success_message']);
      },

  });
}


