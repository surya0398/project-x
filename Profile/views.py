# *************************************************************************************************************************************************************
# This python file is a veiw file for following app. It renders all the needed data in index.html and new_index.html which are under templates/Profile. 
# *************************************************************************************************************************************************************
from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from main_project.settings import STATIC_DIR
from Blog.models import BlogPost
from .models import (AboutMe, ContactInformation, Introduction, Subscription,
                    Projects, Resume, Product_list)
from datetime import date, timedelta
import datetime
import random
from django.utils.safestring import mark_safe
from django.template import Context
from django.template.context import make_context
import re
import urllib.request
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from elasticsearch import Elasticsearch
import json
import heapq
from django.contrib import messages
from django.http import HttpResponseRedirect
import logging
from .utils import get_graph_data,get_statistical_data,get_tweet_data,convert_tweet_data_to_csv

# *************************************************************************************************************************************************************
tv_server_ip_address = 'localhost'
tv_server_port = 9200

commercial_server_ip_address = 'localhost'
commercial_server_port = 9200

class IndexView(TemplateView):
    template_name = 'Profile/index.html'

#     Function :
#                   get_context_data(**kwargs): It returns the context which is to be rendered in the template "Profile/index.html"
#     Input : 
#                   **kwargs : 
#     Output :
#                   it return the context that is dict contating all the data that will be displayed in the index.html.


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Generates context for the blurb
        if Introduction.objects.first() is not None:
            blurb = Introduction.objects.latest()
        else:
            blurb = 'Placeholder'

        context['blurb'] = blurb.intro

        # Generates context for the about me section
        if AboutMe.objects.first() is not None:
            about_me = AboutMe.objects.first()
        else:
            about_me = AboutMe.objects.none()

        context['current_product'] = Product_list.objects.first()
        context['all_products'] = Product_list.objects.all()

        context['about_me'] = about_me

        # Generates context for the project section
        context['projects'] = Projects.objects.all()

        # Generate the context for the most recent blogpost preview
        context['recent_blogpost'] = BlogPost.objects.first()

        graph_data = get_graph_data(productname="pocari_sweat")
        context['graph_data'] = graph_data
        # print (context['graph_data'])

        statistical_data = get_statistical_data()
        context['statistical_data'] = statistical_data
        print (context['statistical_data'])
        return context
# *************************************************************************************************************************************************************
def ResumeView(request):
    try:
        resume = Resume.objects.all().first()
        response = HttpResponse(resume.resume_pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="foo.pdf"'
    except:
        response = HttpResponse('Error')
    return response
# *************************************************************************************************************************************************************
#     Function :
#                   SubscriptonHandler(request): It writes the new user subscrption for the webapp on web database.
#     Input : 
#                   request :
#     Output :      
#                   Returns the Json Response with appropriate success_message.


def SubscriptionHandler(request):

    if request.method == 'POST' and request.is_ajax():
        user_email = request.POST['user_email']
        Subscription.objects.create(user_email=user_email)
        return JsonResponse({'success_message': 'Thanks for subscribing!'})

    return JsonResponse({'success_message': 'Sorry! some error occured'})
# *************************************************************************************************************************************************************
#     Function :
#                   SubscriptonHandler_product(request): It writes the new product in product list in elastic search database. So that product on which twitter data is to be scraped can be extracted in real time.
#     Input : 
#                   request :
#     Output :      
#                   Returns the Json Response with appropriate success_message.

def SubscriptionHandler_product(request):
    ip_address = 'localhost'
    Port = 9200
    es = Elasticsearch([ip_address],port=Port)
    if es.ping():
        success_message = "Thanks for subscribing!"
        if request.method == 'POST' and request.is_ajax():
            product_name = request.POST['product_name']
            product_search_name = '_'.join(product_name.split(" "))
            product_search_name = product_search_name.lower()
            Product_list.objects.create(product_name=product_name,product_search_name=product_search_name)
            body_dict = {"product_name":product_name,"filter_list":[product_name],"index_name":product_search_name}
            product_list_index_name = "product_list"
            id_value = len(Product_list.objects.all())
            res = es.index(index=product_list_index_name,doc_type="_doc",id=id_value,body = json.dumps(body_dict))
            print ("..................\n",res)
    else :
        success_message = "Database server error! Product can't be subscribed."

    return JsonResponse({'success_message': 'Thanks for subscribing!'})
# *************************************************************************************************************************************************************
#     Function :
#                   csv_download(request): This function returns the data in row_list form so that it can be directly converted to csv file via the script calling this function.
#     Input : 
#                   request :
#     Output :      
#                   Returns the Json Response with csv_data in it.

@csrf_exempt
def csv_download(request):
    print ("hello you are at csvdownload view function")
    length = 1
    product_search_name = "pocari"
    if request.method == 'POST':
        length = int(request.POST['length'])
        product_search_name = request.POST['product_search_name']
    print (",,.,,,,,,",product_search_name,",,,,,,,,,,")
    tweet_data = get_tweet_data(length = length,productname=product_search_name)
    csv_data = convert_tweet_data_to_csv(tweet_data)
    return JsonResponse({'csv_data':csv_data})
# *************************************************************************************************************************************************************
#     Function :
#                   load_product_data(request): This function is called by a script in index.html whenver user clicks on to see data of a different product. So It renders template "Profile/new_index.html" with all the data for a different product and then script places this data inside the index.html.
#     Input : 
#                   request
#     Output :      
#                   Renders the new_index.html with context
#     Note : 
#                   Here you should note that new_index.html is just the collection of div containers and not complete html file. So script dynamically loads product data in form of div containers and then places them in appropriate places.

@csrf_exempt
def load_product_data(request):
    # print ("hello you are at csvdownload view function")
    context = {}
    if Introduction.objects.first() is not None:
        blurb = Introduction.objects.latest()
    else:
        blurb = 'Placeholder'
    context['blurb'] = blurb.intro
    # Generates context for the about me section
    if AboutMe.objects.first() is not None:
        about_me = AboutMe.objects.first()
    else:
        about_me = AboutMe.objects.none()
    context['about_me'] = about_me
    # Generates context for the project section
    context['projects'] = Projects.objects.all()
    # Generate the context for the most recent blogpost preview
    context['recent_blogpost'] = BlogPost.objects.first()
    context['message'] = "hello"
    product_search_name = "pocari"
    if request.method == 'POST':
        product_search_name = request.POST['product_search_name']
    context['secondmessage'] = "helllooo"
    selected_product = Product_list.objects.filter(product_search_name=product_search_name).first()
    context['current_product'] = selected_product
    print ("...................",selected_product,"...........")
    context['all_products'] = Product_list.objects.all()
    print(" .................",product_search_name,"................")
    graph_data = get_graph_data(productname = product_search_name)
    context['graph_data'] = graph_data
    statistical_data = get_statistical_data(product_index_name = product_search_name)
    context['statistical_data'] = statistical_data
    print (context['statistical_data'])
    return render(request,'Profile/new_index.html',context)
# *************************************************************************************************************************************************************
#     Function :
#                   upload_csv(request):It is called by script function in index.html and it reads the csv file, process it and then uploads the TV or commercial data on elastic search server.  
#     Input : 
#                   request
#     Output :      
#                   Returns the Json Response with aprroprate message.
#     Note : 
#                   csv uploaded has to be in the particular format which is readable to this function.For additional datatype csv data add under elif of ("datatypename" == "TV data")  .

@csrf_exempt
def upload_csv(request):
    data = {}
   
    try:
        print (request.FILES)
        csv_file = request.FILES["file"]
        product_name = request.POST["productname"]
        datatypename = request.POST["datatypename"]
        print (product_name)
        product = Product_list.objects.filter(product_name=product_name).first()
        product_index_name = getattr(product,'product_search_name')
        print (product_index_name)
        if not csv_file.name.lower().endswith('.csv'):
            # messages.error(request,'File is not CSV type')
            return JsonResponse({'success_message':'File is not CSV type'})
        #if file is too large, return
        print (1)
        if csv_file.multiple_chunks():
            # messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return JsonResponse({'success_message':'Sorry! File is too big'})
        file_data = csv_file.read().decode("shift_jisx0213")     
        lines = file_data.split("\n")
        lines = lines[1:]
        #loop over the lines and save them in db. If error , store as string and then display
        print (2)
        Data_dict = {}
        dates = []
        for line in lines:
            if datatypename == "Commercial Data":
                try:
                    fields = line.split(",")
                    date = re.sub('/','-',fields[0].replace('"',''))
                    station = fields[1].replace('"','')
                    time_volume = fields[9].replace('"','')
                    time_volume = int(time_volume)
                    
                    if date not in Data_dict:
                        date_dict = {}
                        date_dict["total_time_volume"] = time_volume
                        date_dict["station_list"] = [station]
                        Data_dict[date] = date_dict
                        dates.append(date)
                        
                    else:
                        Data_dict[date]["total_time_volume"] += time_volume
                        Data_dict[date]["station_list"].append(station)
                        

                except Exception as e:
                    print (e)

            elif datatypename == "TV Data":
                try:
                    fields = line.split(",")
                    date = re.sub('/','-',fields[0].replace('"',''))
                    date_list = date.split("-")
                    if len(date_list[1]) == 1:
                        date_list[1] = "0"+date_list[1]
                    if len(date_list[2]) == 1:
                        date_list[2] = "0"+date_list[2]
                    date = date_list[0]+"-"+date_list[1]+"-"+date_list[2]
                    station = fields[2].replace('"','')
                    display_time = fields[17].replace('"','')
                    time_list = display_time.split(":")
                    time_volume = int(time_list[0])*3600+int(time_list[1])*60+int(time_list[2])
                    if date not in Data_dict:
                        date_dict = {}
                        date_dict["total_time_volume"] = time_volume
                        date_dict["station_list"] = [station]
                        Data_dict[date] = date_dict
                        dates.append(date)
                        
                    else:
                        Data_dict[date]["total_time_volume"] += time_volume
                        Data_dict[date]["station_list"].append(station)
                        

                except Exception as e:
                    print (e)

            



        print (Data_dict)

        if datatypename == "Commercial Data":
            ip_address = commercial_server_ip_address
            Port = commercial_server_port
        else :
            ip_address = tv_server_ip_address
            Port = tv_server_port

        es = Elasticsearch([ip_address],port=Port)
        if es.ping():
            if datatypename == "Commercial Data":
                title = "_commercial_data"
            elif datatypename == "TV Data":
                title = "_tv_data"
            index_name = product_index_name+title
            try:
                res = es.search(index=index_name,body={'query':{'match_all':{}}})
                current_id = res['hits']['total']['value'] + 1
            except:
                current_id = 1
            for date in dates:
                body_dict = {"date":date,"data":Data_dict[date]}
                print (body_dict)
                res = es.index(id=current_id,index=index_name,body=json.dumps(body_dict),doc_type= 'doc')
                print(res)  
                current_id += 1                

        return JsonResponse({'success_message':'File uploaded Successfully to database.'})

    except Exception as e:
        # logging.getLogger("error_logger").error("Unable to upload file. "+repr(e))
        # messages.error(request,"Unable to upload file. "+repr(e))
        return JsonResponse({'success_message':'Sorry! some unexpected error occured.'})
# *************************************************************************************************************************************************************
