# *****************************************************************************************************************************************************************
# This file contains the utility functions on for collecting and processing datat stored in elastic easrch server. These functions are called by function in views.
# *****************************************************************************************************************************************************************
import heapq
from datetime import date, timedelta
import datetime
import random
from django.utils.safestring import mark_safe
from django.template import Context
from django.template.context import make_context
import re
import urllib.request
import json
from elasticsearch import Elasticsearch
import json
import logging

# *************************************************************************************************************************************************************

# These are the main functions which are called from the view to support it:
#  1. Class Max_heap 
#  2. function get_tweet_data
#  3. function get_graph_data
#  4. function get_statistical_data
#  5. function convert_tweet_data_to_csv
# So to start understanding the code please start from understanding the above.

# *************************************************************************************************************************************************************
twitter_data_server = "http://localhost:9200/"
tv_data_server = "http://localhost:9200/"
commercial_data_server = "http://localhost:9200/"


elastic_search_urls = []
elastic_search_urls.append(twitter_data_server)
elastic_search_urls.append(tv_data_server)
elastic_search_urls.append(commercial_data_server)
# The Max_heap class is a max heap for storing the values accoring to their key. Like say for value ("SMS DATA TECH") with key 34, where 34 representing its priority level.

class Max_heap:

	def __init__(self):
		self.H = []
		heapq.heapify(self.H)

#     Function :
# 					push(key,value)
#     Input : 
#     				key : This is basically a priority level which decides which would be the max element.
#     				value : This is represents the element which would be stored in heap. Like say value can be ("Japan") or ("India") represeting particular info.  
#     Output :
# 					None
 
	def push(self,key,value):
		heapq.heappush(self.H,(-1*key,value))

 
#     Function :
# 					pop()
#     Input : 
#     				None
#     Output :
#     				None
 


	def pop(self):
		heapq.heappop(self.H)

 
#     Function :
# 					get(number_of_elements = n)
#     Input : 
#     				number_of_elements : number of top elements to be returned from heap.  		
#     Output :
#     				List of elements where each element is a tuple of (value,key). 
#     Notes : 
#					It has been implemented in different way to hande duplicate values. 


	def get(self,number_of_elements = 1):
		count = 1
		item_list = []
		duplicate_dict = {}
		while len(self.H)>0 :
			item = self.H[0]
			heapq.heappop(self.H)

			if count > number_of_elements:
				break

			if item[1] not in duplicate_dict:
				duplicate_dict[item[1]]=1
				count += 1
				key = -1*item[0]
				value = item[1]
				item_list.append((value,key))
			else :
				continue
            
			# print (item_list)
		return item_list


# ************************************************************************************************************************************************************************

#     Function :	
# 					get_tweet_data(length,productname) : Collects the twitter tweets from elastic search database and return them as dict containing twitter tweets with their other properties.
#     Input : 
#     				length : It describes the period over which tweet data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					productname : its includes the product of whom twiiter data is required.
#     Output :
#     				returns a dict containing all the tweets in a dict format like given below.
#                     tweet_data_dict = {
#                     	"2019-08-13":{
#                     		"tweet_text_list":[tweet1_text,tweet2_text,.....]
#                     		"timestamp_list":[tweet1_time,tweet2_time,.....]
#                     		...........................
#                     	}
#                     	"2019-08-12":{
#                     		"tweet_text_list":[tweet1_text,tweet2_text,.....]
#                     		"timestamp_list":[tweet1_time,tweet2_time,.....]
#                     		...........................
#                     	}
#                     	............
#                     }
# 
#	  Notes : Tweet dict is in this format so that data can be easily converted to csv format later in views function csv_download

def get_tweet_data(length = 30,productname="pocari"):
    tweet_data_dict ={}
    tweet_data = []
    dates = get_dates(length,marksafe=False)
    urls = []

    # main_url_string,name_string,qury_url_string they are the string which will be used for building the final url string for fetching tweet data.
    # main_url_string : here localhost represent the ip address of the elastic search server and 9200 represent the port number of the same.
    # if you are using the elastic search server which is not setup in your local host then replace the localhost with the public ip address of that server and same follows for port number in case of change. 
    
    main_url_string = elastic_search_urls[0]
    
    # name_string : this constitues the part of index by which tweet data is to be searched.
    
    name_string ="_tweet_data"
    
    # query_url_string : This is the query via which data will be search in the particular index of elastic search database. For now below qury means search the items in a index and return only at max 200 items. 
    
    query_url_string = "/_search?pretty=true&q=date:"

    # Below for loop is to create url list from which data will be scraped later.  
    # url = [
    # 	"http://localhost:9200/pocari_sweat_tweet_data_2019-08-13/_search?pretty=true&q=*:*&size=200"
    # 	"http://localhost:9200/pocari_sweat_tweet_data_2019-08-12/_search?pretty=true&q=*:*&size=200"
    # 	.................
    # ]

    for date in dates:
        urls.append(main_url_string+productname+name_string+query_url_string+date)
    # print(urls)

    for url in urls:
        try:
            response = urllib.request.urlopen(url)
            data = json.loads(response.read())
            data = data["hits"]["hits"]
            data = data[0]["_source"]["data"]
            tweet_data.append(data)
        except:
            tweet_data.append(None)
    
    # This is to convert tweet data in format so that it can be converted to csv. Here for each date tweet data is stored column wise rather than earlier row wise form.

    for i,data in enumerate(tweet_data):            
        tweets_text_list = []
        timestamp_list = []
        associated_product_name_list = []
        if data is not None:
            for item in data:
                tweets_text_list.append(item["tweet_text"])
                timestamp_list.append(item["created_at"])
                associated_product_name_list.append(productname)
        # print (len(tweets_text_list))
        tweet_data_dict[dates[i]] = {"tweets_text_list":tweets_text_list,"timestamp_list":timestamp_list,"associated_product_name_list":associated_product_name_list}
    
 
    tweet_data_dict["dates"] = dates
    return tweet_data_dict

# ***********************************************************************************************************************************************************************

#     Function :	
# 					get_dates(length,marksafe) : returns the list of dates for last month, or week, or any range depending upon length. 
#     Input : 
#     				length : It describes the period over which dates is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					marksafe : It is to handle the string attributes when data is rendered.
#     Output :
#     				list of string containing dates
 


def get_dates(length,marksafe=True):
    dates = []
    for i in range(length):
        dt = date.today()-timedelta(i+1)
        # print (dt)
        dt = dt.strftime("%Y-%m-%d")
        dates.append(dt)
    if marksafe:
        dates = mark_safe(dates)
    return dates


#     Function :	
# 					get_tv_airing_time(length,productname) : returns the tv airing data for particular product for last month, or week, or any range depending upon length input. 
#     Input : 
#     				length : It describes the period over which data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					productname : It describes the productname for which data is required
#     Output :
#     				list of integers whre each element is tv airing time volume for corresponding date.
 

def get_tv_airing_time(length,productname):
    urls = []
    dates = get_dates(length,marksafe=False)
    main_url_string = elastic_search_urls[1]
    name_string ="_tv_data"
    query_url_string = "/_search?pretty=true&q=date:"
    for date in dates:
        # print (main_url_string,date,query_url_string)
        urls.append(main_url_string+productname+name_string+query_url_string+date)
    tv_time_volume = []
    # print (".................")
    # print (urls)
    for url in urls:
        try:
            response = urllib.request.urlopen(url)
            data = json.loads(response.read())
            data = data["hits"]["hits"]
            tv_time_volume.append(data[0]["_source"]["data"]["total_time_volume"])

            
        except:
            tv_time_volume.append(0)
    return tv_time_volume

#     Function :	
# 					get_commercial_airing_time(length,productname) : returns the commercial airing data for particular product for last month, or week, or any range depending upon length input. 
#     Input : 
#     				length : It describes the period over which data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					productname : It describes the productname for which data is required
#     Output :
#     				list of integers where each element is commercial airing time volume for corresponding date.
 
def get_commercial_airing_time(length,productname):
    urls = []
    dates = get_dates(length,marksafe=False)
    main_url_string = elastic_search_urls[2]
    name_string ="_commercial_data"
    query_url_string = "/_search?pretty=true&q=date:"
    for date in dates:
        # print (main_url_string,date,query_url_string)
        urls.append(main_url_string+productname+name_string+query_url_string+date)
    commercial_time_volume = []
    # print (".................")
    # print (urls)
    for url in urls:
        try:
            response = urllib.request.urlopen(url)
            data = json.loads(response.read())
            data = data["hits"]["hits"]
            commercial_time_volume.append(data[0]["_source"]["data"]["total_time_volume"])

            
        except:
            commercial_time_volume.append(0)
    return commercial_time_volume

#     Function :	
# 					get_tweets_volume(length,productname) : returns the twitter tweet volume data for particular product for last month, or week, or any range depending upon length input. 
#     Input : 
#     				length : It describes the period over which data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					productname : It describes the productname for which data is required
#     Output :
#     				list of integers where each element is twitter tweet volume for corresponding date.
 
def get_tweets_volume(length,productname):
    # print ("u are there")
    urls = []
    dates = get_dates(length,marksafe=False)
    main_url_string = elastic_search_urls[0]
    name_string ="_tweet_statistical_data"
    query_url_string = "/_search?pretty=true&q=date:"
    for date in dates:
        # print (main_url_string,date,query_url_string)
        urls.append(main_url_string+productname+name_string+query_url_string+date)
    tweets_volume = []
    # print (".................")
    print (urls)
    for url in urls:
        try:
            response = urllib.request.urlopen(url)
            data = json.loads(response.read())
            data = data["hits"]["hits"]
            tweets_volume.append(data[0]["_source"]["data"]["total_count"])
        except:
            tweets_volume.append(0)
    return tweets_volume

#   This is just a dummy function for now. It is only for future implementations.

def get_other_info(length,productname):
    return []

#     Function :	
# 					get_graph_data(productname,length) : returns the graph data to be represented for particular product for last month, or week, or any range depending upon length input. 
#     Input : 
#     				length : It describes the period over which data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					productname : It describes the productname for which data is required
#     Output :
#     				list of integers where each element is twitter tweet volume for corresponding date.
 
def get_graph_data(productname = "pocari",length = 30):
	dates = get_dates(length)
	tv_airing_time = get_tv_airing_time(length,productname)
	commercial_airing_time = get_commercial_airing_time(length,productname) 
	tweets_volume = get_tweets_volume(length,productname)
	other_info = get_other_info(length,productname)
	context = {'dates':dates,'tv_airing_time':tv_airing_time,'commercial_airing_time':commercial_airing_time,'tweets_volume':tweets_volume,'other_info':other_info}
	return context

# ***********************************************************************************************************************************************************************


def tweet_cleaner(tweet):
    tweet = re.sub("#", "", tweet)
    tweet = re.sub("\n"," ",tweet)
    return tweet


#     Function :	
# 					convert_tweet_data_to_csv(tweet_data) : It convert the tweet_data which is in dict form to row_list form or csv form which will then be converted to csv via script later. 
#     Input : 
#     				tweet_data : A dict containing the tweet data for specific period.
#     Output :
#     				ex :
# 					row_list = [
# 						['Tweet','Timestamp','Associated Product'],
# 						['I like pocari sweat','2019-08-13','Pocari Sweat'],
# 						['Pocari sweat is best','2019-08-13','Pocari Sweat'],
# 						.......................
# 					]					
 

def convert_tweet_data_to_csv(tweet_data = {}):
    row_list = []
    row_list.append(['Tweet','Timestamp','Associated Product'])
    for date in tweet_data["dates"]:
        tweets_text_list = tweet_data[date]["tweets_text_list"]
        timestamp_list = tweet_data[date]["timestamp_list"]
        associated_product_name_list = tweet_data[date]["associated_product_name_list"]
        for i,tweet in enumerate(tweets_text_list):
            tweet = tweet_cleaner(tweet)
            row_list.append([tweet,str(timestamp_list[i]),str(associated_product_name_list[i])])
    # print (row_list)
    return row_list

# ***********************************************************************************************************************************************************************

#     Function :	
# 					get_statisticl_urls : It return the list of urls for elastic search server from where statistical data will be pulled for particular date.
#     Input : 
#     				product_index_name : It is the name of the product index for which data will be stored.
# 					length : It describes the period over which data is required. for ex. length = 30 means entire last month datat is required, length = 7 means last week data is required.
# 					data_name : It describes the type of data to be pulled from elastic serach server.
#     Output :
#     				returns list of urls 
def get_statistical_urls(product_index_name,length,data_name):
    dates = get_dates(length,marksafe=False)
    main_url_string = elastic_search_urls[0]
    query_url_string = "/_search?pretty=true&q=date:"
    url_list = []
    for date in dates:
        url_list.append(main_url_string+product_index_name+'_'+data_name+query_url_string+date)
    return url_list

#     Function :	
# 					get_average : A utility function for below analyse_parameter function. It takes the sum of all the values for list and list2 and then return the division of same.

def get_average(list1,list2):
    N = len(list1)
    numerator = 0
    denominator = 0
    for i in range(N):
        numerator += list1[i]
        denominator += list2[i]
    if denominator != 0:
        return float(numerator)/float(denominator)
    else:
        return 0

#     Function :	
# 					get_overall(main_list,number_of_elements,key_name,val_name,extend,knowledge): It reads through sublists present in main_list, pushes elements to heap while acting on certain input extend and knowledge, and then returns top elements on the basis of input number_of_elements.  
#     Input : 
#     				main_list : Its is the list of sub-list where each sub-list contains dict elements. ex:
# 					mainList = [
# 						[
# 							{"name":"Naruto","value":"45",...},
# 							{"name":"Sauske","value":"65",...},f
# 							......
# 						],
# 						[

# 							{"name":"Tsunade","value":"49",...},
# 							{"name":"Madara","value":"55",...},
# 							......
# 						],
# 						..................
# 					]
# 					number_of_elements: It represents how many top elements are to be returned.
# 					key_name: It is a string which represents which element of dict will act as a key.
# 					val_name: It is a string which represents which element of dict represent that dict.
# 					extend: If it is true then while pushing in heap if element duplicate is found then the key values of all duplicate will be added else the maximum of the key value of duplicates will stored.
# 					knowledge: When elements are pushed in heap only key_name and val_name parameters are stored for a elemt in heap. So to restore there other properties if they have knowledge equal to true is kept else false is kept.   

#     Output :
#     				returns list of top elements

def get_overall(main_list=[],numbers_of_elements = 5,key_name = "value",val_name = "name",extend = True,knowledge = True):
    Heap = Max_heap()
    result_list = []
    duplicate_dict = {}
    knowledge_dict = {}
    for sublist in main_list:
        if sublist != None:
            for item in sublist:
                if knowledge:
                    if item[val_name] not in knowledge_dict:
                        knowledge_dict[item[val_name]] = item
                if extend:
                    if item[val_name] not in duplicate_dict:
                        duplicate_dict[item[val_name]] = item[key_name]
                    else:
                        duplicate_dict[item[val_name]] += item[key_name]
                else:
                    if item[val_name] not in duplicate_dict:
                        duplicate_dict[item[val_name]] = item[key_name]

                Heap.push(duplicate_dict[item[val_name]],item[val_name])

    top_elements = Heap.get(number_of_elements = numbers_of_elements)
    # print(top_elements)

    for items in top_elements:
        if knowledge:
            item_dict = knowledge_dict[items[0]]
        else:
            item_dict = {}
            item_dict[val_name] = items[0]
        item_dict[key_name] = items[1]
        result_list.append(item_dict)
    
    return result_list

#     Function :	
# 					get_percentage : It is a utility function which returns list of dict items containing top entities depending upon number_of_elements variable  
def get_percentage(list_dict,numbers_of_elements = 7):
    result_list = []
    total = 0
    for item in list_dict:
        total += item["entity_frequency"]

    count  = 1

    for item in list_dict:
        if count > numbers_of_elements:
            break
        entity_dict = item
        if total != 0:
            entity_dict["frequencey_percentage"] = (float(item["entity_frequency"])/float(total))*100
        else:
            entity_dict["frequencey_percentage"] = 0
        result_list.append(entity_dict)
        count += 1
    return result_list

#     Function :	
# 					analyse_parameters(statistical_data): It reads through all the data of each date and then provides result on the basis of overall statistical analysis.
#     Input : 
#     				statistical_data : It is the list of dict elements where each dict element represents the individual statistical data of each date.  
# 					mainList = [
# 						{
# 							....,
# 							....,
# 							"_source":{
# 								"total_count": 120,
# 								"positive_count": 70,
# 								"negative_count": 30,
# 								"neutral_count": 20,
# 								"retweet_count": 4556,
# 								..........
# 							}
# 						},
# 						{
# 							......
# 						},
# 						.......
# 					]
#     Output :
#     				returns dict which contains numerous paramerts from statistical analysis on statistical_data

def analyse_parameters(statistical_data):
    total_count_list = []
    positive_count_list = []
    negative_count_list = []
    neutral_count_list = []
    retweet_count_list = []
    original_count_list = []
    most_active_users = []
    most_popular_users = []
    most_found_entities = []
    most_retweeted_tweets = []
    most_liked_tweets = []
    most_positive_tweets = []
    likes_per_tweet_count_list = []
    retweet_per_tweet_count_list = []

    for data in statistical_data:
        try:
            source_data = data[0]["_source"]
            source_data = source_data['data']
            # print (".......................................\n",source_data)
            total_count_list.append(source_data["total_count"])
            positive_count_list.append(source_data["positive_count"])
            negative_count_list.append(source_data["negative_count"])
            neutral_count_list.append(source_data["neutral_count"])
            retweet_count_list.append(source_data["retweet_count:"])
            original_count_list.append(source_data["original_count"])
            most_active_users.append(source_data["most_active"])
            most_popular_users.append(source_data["most_popular"])
            most_found_entities.append(source_data["most_entities"])
            most_retweeted_tweets.append(source_data["most_retweeted"])
            most_liked_tweets.append(source_data["most_liked"])
            most_positive_tweets.append(source_data["most_positive"])
            likes_per_tweet_count_list.append(source_data["likes_per_tweet_count"])
            retweet_per_tweet_count_list.append(source_data["retweet_per_tweet_count"])

        except:
            total_count_list.append(0)
            positive_count_list.append(0)
            negative_count_list.append(0)
            neutral_count_list.append(0)
            retweet_count_list.append(0)
            original_count_list.append(0)
            most_active_users.append(None)
            most_popular_users.append(None)
            most_found_entities.append(None)
            most_retweeted_tweets.append(None)
            most_liked_tweets.append(None)
            most_positive_tweets.append(None)
            likes_per_tweet_count_list.append(0)
            retweet_per_tweet_count_list.append(0)


    statistical_data_dict = {}
    statistical_data_dict["positive_count_percentage"] = round(get_average(positive_count_list,total_count_list)*100,2)
    statistical_data_dict["negative_count_percentage"] = round(get_average(negative_count_list,total_count_list)*100,2)
    statistical_data_dict["neutral_count_percentage"] = round(get_average(neutral_count_list,total_count_list)*100,2)
    statistical_data_dict["retweet_count_percentage"] = round(get_average(retweet_count_list,total_count_list)*100,2)
    statistical_data_dict["original_count_percentage"] = round(get_average(original_count_list,total_count_list)*100,2)
    statistical_data_dict["likes_per_tweet_count_percentage"] = round(get_average(likes_per_tweet_count_list,total_count_list),2)
    statistical_data_dict["retweet_per_tweet_count_percentage"] = round(get_average(retweet_per_tweet_count_list,total_count_list),2)
    statistical_data_dict["overall_active_users"] = get_overall(main_list = most_active_users,numbers_of_elements=5)
    statistical_data_dict["overall_popular_users"] = get_overall(main_list = most_popular_users,numbers_of_elements=5,extend=False)
    statistical_data_dict["overall_found_entities"] = get_percentage(get_overall(main_list = most_found_entities,numbers_of_elements=10,key_name = "entity_frequency",val_name="entity_name"))
    statistical_data_dict["overall_retweeted_tweets"] = get_overall(main_list = most_retweeted_tweets,numbers_of_elements=5,key_name="value",val_name="text",extend=False,knowledge=False)
    statistical_data_dict["overall_liked_tweets"] = get_overall(main_list = most_liked_tweets,numbers_of_elements=5,key_name="value",val_name="text",extend=False,knowledge=False)
    statistical_data_dict["overall_positive_tweets"] = get_overall(main_list = most_positive_tweets,numbers_of_elements=5,key_name="value",val_name="text",extend=False,knowledge=False)
    return statistical_data_dict

#     Function :	
# 					get_statistical_data(product_index_name,length): It collects data from elastic search server and then sends it to analyse_parameters function for statistical analysis.
#     Input : 
#     				product_index_name : 
# 					length : 
# 					]
#     Output :
#     				returns dict which contains numerous paramerts from statistical analysis on statistical_data

def get_statistical_data(product_index_name = "pocari_sweat",length = 30):
    required_urls = get_statistical_urls(product_index_name,length,'tweet_statistical_data')
    # sprint (required_urls)
    statistical_data = []
    for url in required_urls:
        try:
            response = urllib.request.urlopen(url)
            data = json.loads(response.read())
            statistical_data.append(data["hits"]["hits"])
        except:
            statistical_data.append(None)
    # print (statistical_data)
    statistical_data_dict = analyse_parameters(statistical_data)
    return statistical_data_dict


# ***********************************************************************************************************************************************************************
