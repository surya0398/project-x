from django.urls import path

from .views import upload_csv,IndexView, SubscriptionHandler, ResumeView, csv_download, load_product_data, SubscriptionHandler_product

app_name = 'Profile'

urlpatterns = [
    path(
        '',
        IndexView.as_view(),
        name='index',
        ),
    path(
        'BNResume/',
        ResumeView,
        name='resume',
        ),
    path(
        'Subscription/',
        SubscriptionHandler,
        name='subscription',
        ),
    path(
        'Product_Subscription/',
        SubscriptionHandler_product,
        name='product-subscription',
        ),
    path(
        'csv_download/',
        csv_download,
        name='csv_download',
        ),
    path(
        'load_product_data/',
        load_product_data,
        name='load_product_data',
        ),
    path(
        'upload_csv',
        upload_csv,
        name='upload_csv'
        ),
]
