from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path(
        'admin/',
        admin.site.urls,
        ),
    path(
        '',
        include('Blog.urls'),
        ),
    path('profile/',
        include('Profile.urls'),
        ),
    path('custom_search/',
        include('custom_search.urls'),
        ),
    path('django_plotly_dash/', 
        include('django_plotly_dash.urls'),
        ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
